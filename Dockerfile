FROM maven:3-jdk-11 AS build
COPY backend/src project/backend/src
COPY backend/pom.xml project/backend
RUN mvn -f /project/backend/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build project/backend/target/v2l-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]
